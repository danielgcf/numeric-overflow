package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void ThousandsNeedsApproval() {
        Main app = new Main();
        boolean res = app.approval(Amount.of(BigInteger.valueOf(1000)));
        assertTrue(res,() -> "1000 needs approval");
    }

    @Test
    public void FileHundredsDoesNotNeedApproval() {
        Main app = new Main();
        boolean res = app.approval(Amount.of(BigInteger.valueOf(500)));
        assertFalse(res,() -> "500 does not need approval");
    }

    @Test
    public void AmountCanNotHaveNullValue() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            boolean res = app.approval(Amount.of(null));
            assertTrue(res,() -> "Amount can not have null value");
        });
    }
}
