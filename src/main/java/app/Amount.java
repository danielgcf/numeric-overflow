package app;

import java.math.BigInteger;

public class Amount {

    private static BigInteger INT_MIN_VALUE = BigInteger.ZERO;
    private static BigInteger INT_MAX_VALUE = BigInteger.valueOf(Integer.MAX_VALUE);

    private final BigInteger value;

    private Amount(BigInteger value) {
        this.value = value;
    }

    public BigInteger getValue() {
        return value;
    }

    public static Amount of(BigInteger amount) {
        if (amount == null || amount.compareTo(INT_MIN_VALUE) < 0 || amount.compareTo(INT_MAX_VALUE) > 0) {
            throw new IllegalArgumentException();
        }
        return new Amount(amount);
    }

}
