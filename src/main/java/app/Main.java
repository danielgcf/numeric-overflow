package app;

import java.math.BigInteger;
import java.util.Arrays;

public class Main {

    private Amount threshold = Amount.of(BigInteger.valueOf(1000));

    public static void main(String[] args) {

        Main app = new Main();

        Amount amount = Amount.of(BigInteger.valueOf(999));
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = Amount.of(BigInteger.valueOf(2000));
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(Amount amount){
       if(amount.getValue().compareTo(threshold.getValue()) >= 0) {
           return true;
       }
       return false;
   }

}
